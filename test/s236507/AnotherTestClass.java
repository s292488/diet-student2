package s236507;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import diet.Customer;
import diet.Food;
import diet.Restaurant;
import diet.Takeaway;

public class AnotherTestClass {
	
	Food f;
	
	@Before
	public void setUp() {
		this.f = new Food();
	}
	
	@Test
	public void testR2_Products() {
		f.defineProduct("Cracker", 111, 2.6, 17.2, 3.5);
		
		assertTrue(f.getProduct("Cracker").getCalories() == 111);
		assertTrue(f.getProduct("Cracker").getProteins() == 2.6);
		assertTrue(f.getProduct("Cracker").getCarbs() == 17.2);
		assertTrue(f.getProduct("Cracker").getFat() == 3.5);
		assertFalse(f.getProduct("Cracker").per100g());
	}
	
	@Test
	public void testR5_Restaurants() {
		Takeaway t = new Takeaway(f);
		Restaurant r1 = t.addRestaurant("Napoli");

		assertNotNull("Missing restaurant", r1);
		assertEquals("Wrong restaurant name", "Napoli", r1.getName());

		r1.setHours("08:00", "14:00", "19:00", "23:59");
		Restaurant r2 = t.addRestaurant("Roma");
		r2.setHours("08:45", "13:30", "18:20", "23:00");
		
		Collection<String> restaurants = t.restaurants();
		assertEquals("Wrong number of restaurants", 2, restaurants.size());

		assertTrue("Should be open", r1.isOpenAt("12:00"));
		assertFalse("Should be closed", r2.isOpenAt("17:00"));
	}
	
	@Test
	public void testR6_Users() {
		Takeaway takeaway = new Takeaway(f);

		// Registering new users
		Customer c1 = takeaway.registerCustomer("Ralph", "Fiennes", "r.fiennes@gmail.com", "333413493");
		
		takeaway.registerCustomer("Ian", "McKellen", "i.mckellen@gmail.com", "124882578");
		takeaway.registerCustomer("Maggie", "Smith", "m.smith@gmail.com", "3647851225");
		
		Collection<Customer> customers = takeaway.customers();

		assertNotNull("Missing customers", customers);
	}

}
