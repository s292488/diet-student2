package s236507;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import diet.Customer;
import diet.Food;
import diet.Menu;
import diet.Order;
import diet.Restaurant;
import diet.Takeaway;

public class YetAnotherTestClass {

	Food foods;
	Takeaway takeaway;
	private Menu menu1, menu2, menu3;

	@Before
	public void setUp() {
		foods = new Food();
		takeaway = new Takeaway(foods);

		foods.defineRawMaterial("Sugar", 400, 0, 100, 0);
		foods.defineRawMaterial("Mais", 70, 2.7, 12, 1.3);
		foods.defineRawMaterial("Pasta", 350, 12, 72.2, 1.5);
		foods.defineRawMaterial("Oil", 900, 0, 0, 100);
		foods.defineRawMaterial("Nutella", 530, 6.8, 56, 31);
		foods.defineRawMaterial("Eggs", 120, 6.8, 56, 31);
		foods.defineRawMaterial("Flour", 100, 6.8, 56, 31);
		foods.defineRawMaterial("Sausage", 500, 15.8, 20, 4.1);
		foods.defineRawMaterial("Tomato sauce", 120, 6.8, 56, 31);
		foods.defineRawMaterial("Minced meat", 450, 6.8, 56, 31);
		foods.defineRawMaterial("Mozzarella", 250, 6.8, 56, 31);
		foods.defineRawMaterial("Olives", 60, 6.8, 56, 31);
		foods.defineRawMaterial("Pesto", 120, 6.8, 56, 20);
		foods.defineRawMaterial("Onion", 40, 3, 20.3, 1);
		foods.defineRawMaterial("Prosciutto Crudo", 420, 6.8, 56, 31);
		foods.defineRawMaterial("Carrots", 60, 6.8, 56, 31);
		foods.defineRawMaterial("Red wine", 60, 6.8, 56, 31);
		foods.defineRawMaterial("Bacon", 530, 6.8, 56, 31);
		foods.defineRawMaterial("Garlic", 34, 4.1, 21.2, 2);
		foods.defineRawMaterial("Cream", 530, 6.8, 56, 31);
		foods.defineRawMaterial("Gorgonzola", 300, 6.8, 56, 31);
		foods.defineRawMaterial("Walnuts", 220, 6.8, 56, 31);
		foods.defineRawMaterial("Prosciutto Cotto", 400, 6.8, 56, 31);
		foods.defineRawMaterial("Mortadella", 420, 6.8, 56, 31);

		foods.defineProduct("Beer 0.5l", 40, 0.5, 0.2, 0.05);
		foods.defineProduct("Grissini", 20, 0.5, 0.2, 0.05);
		foods.defineProduct("Biscuits", 150, 2.0, 10.2, 1.4);
		foods.defineProduct("Amaro", 10, 0.6, 0.25, 0.1);
		foods.defineProduct("Wine 0.5l", 40, 0.5, 1.2, 0.05);
		foods.defineProduct("Water bottle 0.33l", 5, 0.1, 0.2, 0.05);
		foods.defineProduct("Orange Juice 0.4l", 80, 0.5, 2.2, 0.05);
		foods.defineProduct("Crackers", 111, 2.6, 17.2, 3.5);

		// RECIPES
		foods.createRecipe("Pasta and Nutella").addIngredient("Pasta", 70).addIngredient("Nutella", 30);

		foods.createRecipe("Pasta al Ragu").addIngredient("Pasta", 350).addIngredient("Onion", 100)
				.addIngredient("Garlic", 40).addIngredient("Tomato sauce", 250).addIngredient("Red wine", 50)
				.addIngredient("Carrots", 150).addIngredient("Bacon", 200).addIngredient("Minced meat", 400);

		foods.createRecipe("Pizza 1").addIngredient("Flour", 150).addIngredient("Oil", 20).addIngredient("Sausage", 100)
				.addIngredient("Tomato sauce", 75).addIngredient("Pesto", 40).addIngredient("Mozzarella", 50);

		foods.createRecipe("Pizza 2").addIngredient("Flour", 150).addIngredient("Oil", 20).addIngredient("Cream", 75)
				.addIngredient("Gorgonzola", 175).addIngredient("Walnuts", 50);

		foods.createRecipe("Pizza 3").addIngredient("Flour", 160).addIngredient("Oil", 30)
				.addIngredient("Mozzarella", 200).addIngredient("Prosciutto Crudo", 140).addIngredient("Olives", 65);

		foods.createRecipe("Meatballs").addIngredient("Tomato sauce", 400).addIngredient("Eggs", 50)
				.addIngredient("Onion", 175).addIngredient("Minced meat", 500);

		// MENUS
		menu1 = foods.createMenu("M1").addRecipe("Pasta and Nutella", 50).addProduct("Crackers");

		menu2 = foods.createMenu("M2").addRecipe("Pizza 1", 350).addProduct("Beer 0.5l");

		menu3 = foods.createMenu("M3").addRecipe("Pasta al Ragu", 320).addProduct("Wine 0.5l").addProduct("Biscuits");
	}

	@Test
	public void testR7_Orders() {
		Customer u1 = takeaway.registerCustomer("Ralph", "Fiennes", "r.fiennes@gmail.com", "333413493");
		Restaurant r1 = takeaway.addRestaurant("Napoli");
		r1.setHours("08:00", "14:00", "19:00", "23:59");

		r1.addMenu(menu1);
		r1.addMenu(menu2);
		r1.addMenu(menu3);

		Order o1 = takeaway.createOrder(u1, "Napoli", "19:30");// r1
		assertNotNull("Missing order", o1);
	}

}
