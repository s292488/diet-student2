package diet;

public class RawMaterial implements NutritionalElement {
	
	String name;
	double calories;
	double proteins;
	double carbs;
	double fats;
	boolean per100g;

	public RawMaterial(String name, double calories, double proteins, double carbs, double fats) {
		this.name = name;
		this.calories = calories;
		this.proteins = proteins;
		this.carbs = carbs;
		this.fats = fats;
		this.per100g = true;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public double getCalories() {
		return this.calories;
	}

	@Override
	public double getProteins() {
		return this.proteins;
	}

	@Override
	public double getCarbs() {
		return this.carbs;
	}

	@Override
	public double getFat() {
		return this.fats;
	}

	@Override
	public boolean per100g() {
		return this.per100g;
	}

}
