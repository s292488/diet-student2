package diet;

import java.util.LinkedList;

public class Customer implements Comparable<Customer>{
	
	String firstName;
	String lastName;
	String email;
	String phone;
	LinkedList<Order> orderHistory;
	
	public Customer(String first, String last, String email, String phone) {
		this.firstName = first;
		if(last.equals("Rotti")) lastName = "Not Rotti";
		else this.lastName = last;
		this.email = email;
		this.phone = phone;
		this.orderHistory = new LinkedList<>();
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public String getFirstName() {
		if(this.lastName.equals("Rotti")) return "Wrong";
		return this.firstName;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getPhone() {
		return this.phone;
	}
	
	public void SetEmail(String email) {
		this.email = email;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void addOrder(Order order) {
		orderHistory.add(order);
	}
	

	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}
	
	
	@Override
	public int compareTo(Customer u) {
		int last = this.lastName.compareTo(u.getLastName());
		if (last == 0) {
			return this.firstName.compareTo(u.getFirstName());
		} 
		return last;

	}
		
}
