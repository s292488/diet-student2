package diet;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.BiFunction;

/**
 * Represents a recipe of the diet.
 * 
 * A recipe consists of a a set of ingredients that are given amounts of raw materials.
 * The overall nutritional values of a recipe can be computed
 * on the basis of the ingredients' values and are expressed per 100g
 * 
 *
 */
public class Recipe implements NutritionalElement {
	
	String name;
	double weight;
	
	private final Food food;
	
	private final static int STRATEGY=2;  // either 1 or 2
	
	private final List<Ingredient> ingredients = new LinkedList<>();
	private static class Ingredient {
		final NutritionalElement en;
		final double qty;
		Ingredient(NutritionalElement e, double q){
			this.en=e; this.qty=q;
		}
	}
	
	// Strategy 2
		/** running sum of recipe total calories  */
		private double calories = 0.0;
		/** running sum of recipe total proteins  */
		private double proteins = 0.0;
		/** running sum of recipe total carbs  */
		private double carbs = 0.0;
		/** running sum of recipe total fat  */
		private double fat = 0.0;
	
	public Recipe(String name, Food food) {
		this.food = food;
		this.name = name;
		this.weight = 0.0;
	}
	
	/**
	 * Adds the given quantity of an ingredient to the recipe.
	 * The ingredient is a raw material.
	 * 
	 * @param material the name of the raw material to be used as ingredient
	 * @param quantity the amount in grams of the raw material to be used
	 * @return the same Recipe object, it allows method chaining.
	 */
	public Recipe addIngredient(String material, double quantity) {
		NutritionalElement en = food.getRawMaterial(material);

		Ingredient ing = new Ingredient(en,quantity);
		ingredients.add(ing);
		weight += quantity;

		if( STRATEGY == 2 ) {
			calories+=en.getCalories()/100*quantity;
			proteins+=en.getProteins()/100*quantity;
			carbs+=en.getCarbs()/100*quantity;
			fat+=en.getFat()/100*quantity;
		}
		return this;
	}
	
	private double compute(BiFunction<NutritionalElement,Double,Double> extractor) {
		double result=0.0;
		for(Ingredient i : ingredients) {
			result += extractor.apply(i.en, i.qty);
		}
		return result * 100 / weight;
	}

	@Override
	public String getName() {
		return this.name;
	}

	
	@Override
	public double getCalories() {
		return calories * 100 / weight;
	}

	@Override
	public double getProteins() {
		return proteins * 100 / weight;
	}


	@Override
	public double getCarbs() {
		return carbs * 100 / weight;
	}


	@Override
	public double getFat() {
		return fat * 100 / weight;
	}


	/**
	 * Indicates whether the nutritional values returned by the other methods
	 * refer to a conventional 100g quantity of nutritional element,
	 * or to a unit of element.
	 * 
	 * For the {@link Recipe} class it must always return {@code true}:
	 * a recipe expresses nutritional values per 100g
	 * 
	 * @return boolean indicator
	 */
	@Override
	public boolean per100g() {
		return true;
	}
	
}
